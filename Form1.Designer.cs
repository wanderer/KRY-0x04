﻿namespace KRY_0x04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msgbox = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.msgpathbox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.privkeybox = new System.Windows.Forms.RichTextBox();
            this.pubkeybox = new System.Windows.Forms.RichTextBox();
            this.sha256sumbox = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.privkeypathbox = new System.Windows.Forms.TextBox();
            this.pubkeypathbox = new System.Windows.Forms.TextBox();
            this.msgdetailsbox = new System.Windows.Forms.TextBox();
            this.msgs_differ_label = new System.Windows.Forms.Label();
            this.keygenbutton = new System.Windows.Forms.Button();
            this.threadsbox = new System.Windows.Forms.TextBox();
            this.keysizebutton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_threads_warning = new System.Windows.Forms.Label();
            this.label_keygen_running = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // msgbox
            // 
            this.msgbox.Location = new System.Drawing.Point(12, 12);
            this.msgbox.Name = "msgbox";
            this.msgbox.Size = new System.Drawing.Size(437, 102);
            this.msgbox.TabIndex = 5;
            this.msgbox.Text = "";
            this.msgbox.TextChanged += new System.EventHandler(this.msgbox_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(467, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "load msg";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.load_msg_button_Click);
            // 
            // msgpathbox
            // 
            this.msgpathbox.Location = new System.Drawing.Point(12, 120);
            this.msgpathbox.Name = "msgpathbox";
            this.msgpathbox.ReadOnly = true;
            this.msgpathbox.Size = new System.Drawing.Size(437, 20);
            this.msgpathbox.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(467, 342);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "load privkey";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.load_privkey_button_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(467, 433);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "load pubkey";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.load_pubkey_button_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(558, 120);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 9;
            this.button5.Text = "save msg";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.save_msg_button_Click);
            // 
            // privkeybox
            // 
            this.privkeybox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.privkeybox.Location = new System.Drawing.Point(12, 309);
            this.privkeybox.Name = "privkeybox";
            this.privkeybox.ReadOnly = true;
            this.privkeybox.Size = new System.Drawing.Size(437, 55);
            this.privkeybox.TabIndex = 10;
            this.privkeybox.Text = "";
            // 
            // pubkeybox
            // 
            this.pubkeybox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pubkeybox.Location = new System.Drawing.Point(12, 401);
            this.pubkeybox.Name = "pubkeybox";
            this.pubkeybox.ReadOnly = true;
            this.pubkeybox.Size = new System.Drawing.Size(437, 55);
            this.pubkeybox.TabIndex = 11;
            this.pubkeybox.Text = "";
            // 
            // sha256sumbox
            // 
            this.sha256sumbox.Location = new System.Drawing.Point(12, 203);
            this.sha256sumbox.Name = "sha256sumbox";
            this.sha256sumbox.ReadOnly = true;
            this.sha256sumbox.Size = new System.Drawing.Size(437, 20);
            this.sha256sumbox.TabIndex = 12;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(558, 342);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 13;
            this.button6.Text = "sign";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.sign_button_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(558, 433);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 14;
            this.button7.Text = "verify";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.verify_button_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(467, 268);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 17;
            this.button9.Text = "zip";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.zip_button_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(558, 268);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 18;
            this.button10.Text = "unzip";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.unzip_button_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(727, 342);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 21;
            this.button12.Text = "clean ALL";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.cleanALL_button_Click);
            // 
            // privkeypathbox
            // 
            this.privkeypathbox.Location = new System.Drawing.Point(12, 370);
            this.privkeypathbox.Name = "privkeypathbox";
            this.privkeypathbox.ReadOnly = true;
            this.privkeypathbox.Size = new System.Drawing.Size(437, 20);
            this.privkeypathbox.TabIndex = 23;
            // 
            // pubkeypathbox
            // 
            this.pubkeypathbox.Location = new System.Drawing.Point(12, 462);
            this.pubkeypathbox.Name = "pubkeypathbox";
            this.pubkeypathbox.ReadOnly = true;
            this.pubkeypathbox.Size = new System.Drawing.Size(437, 20);
            this.pubkeypathbox.TabIndex = 24;
            // 
            // msgdetailsbox
            // 
            this.msgdetailsbox.Location = new System.Drawing.Point(12, 146);
            this.msgdetailsbox.Multiline = true;
            this.msgdetailsbox.Name = "msgdetailsbox";
            this.msgdetailsbox.ReadOnly = true;
            this.msgdetailsbox.Size = new System.Drawing.Size(437, 51);
            this.msgdetailsbox.TabIndex = 25;
            // 
            // msgs_differ_label
            // 
            this.msgs_differ_label.AutoSize = true;
            this.msgs_differ_label.BackColor = System.Drawing.SystemColors.HighlightText;
            this.msgs_differ_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgs_differ_label.ForeColor = System.Drawing.Color.Firebrick;
            this.msgs_differ_label.Location = new System.Drawing.Point(464, 204);
            this.msgs_differ_label.Name = "msgs_differ_label";
            this.msgs_differ_label.Size = new System.Drawing.Size(198, 16);
            this.msgs_differ_label.TabIndex = 26;
            this.msgs_differ_label.Text = "msg changed but not saved";
            this.msgs_differ_label.Visible = false;
            // 
            // keygenbutton
            // 
            this.keygenbutton.Location = new System.Drawing.Point(653, 473);
            this.keygenbutton.Name = "keygenbutton";
            this.keygenbutton.Size = new System.Drawing.Size(75, 23);
            this.keygenbutton.TabIndex = 27;
            this.keygenbutton.Text = "keygen";
            this.keygenbutton.UseVisualStyleBackColor = true;
            this.keygenbutton.Click += new System.EventHandler(this.keygen_button_Click);
            // 
            // threadsbox
            // 
            this.threadsbox.Location = new System.Drawing.Point(734, 435);
            this.threadsbox.Name = "threadsbox";
            this.threadsbox.Size = new System.Drawing.Size(39, 20);
            this.threadsbox.TabIndex = 28;
            this.threadsbox.TextChanged += new System.EventHandler(this.threadsbox_TextChanged);
            // 
            // keysizebutton
            // 
            this.keysizebutton.Location = new System.Drawing.Point(653, 433);
            this.keysizebutton.Name = "keysizebutton";
            this.keysizebutton.Size = new System.Drawing.Size(75, 23);
            this.keysizebutton.TabIndex = 29;
            this.keysizebutton.Text = "1024b";
            this.keysizebutton.UseVisualStyleBackColor = true;
            this.keysizebutton.Click += new System.EventHandler(this.keysize_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(650, 417);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "key size";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(731, 417);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "threads";
            // 
            // label_threads_warning
            // 
            this.label_threads_warning.AutoSize = true;
            this.label_threads_warning.Location = new System.Drawing.Point(734, 458);
            this.label_threads_warning.Name = "label_threads_warning";
            this.label_threads_warning.Size = new System.Drawing.Size(0, 13);
            this.label_threads_warning.TabIndex = 32;
            // 
            // label_keygen_running
            // 
            this.label_keygen_running.AutoSize = true;
            this.label_keygen_running.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_keygen_running.ForeColor = System.Drawing.Color.Red;
            this.label_keygen_running.Location = new System.Drawing.Point(734, 480);
            this.label_keygen_running.Name = "label_keygen_running";
            this.label_keygen_running.Size = new System.Drawing.Size(125, 16);
            this.label_keygen_running.TabIndex = 33;
            this.label_keygen_running.Text = "keygen running...";
            this.label_keygen_running.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 508);
            this.Controls.Add(this.label_keygen_running);
            this.Controls.Add(this.label_threads_warning);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.keysizebutton);
            this.Controls.Add(this.threadsbox);
            this.Controls.Add(this.keygenbutton);
            this.Controls.Add(this.msgs_differ_label);
            this.Controls.Add(this.msgdetailsbox);
            this.Controls.Add(this.pubkeypathbox);
            this.Controls.Add(this.privkeypathbox);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.sha256sumbox);
            this.Controls.Add(this.pubkeybox);
            this.Controls.Add(this.privkeybox);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.msgpathbox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.msgbox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "kry-0x04";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox msgbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox msgpathbox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RichTextBox privkeybox;
        private System.Windows.Forms.RichTextBox pubkeybox;
        private System.Windows.Forms.TextBox sha256sumbox;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox privkeypathbox;
        private System.Windows.Forms.TextBox pubkeypathbox;
        private System.Windows.Forms.TextBox msgdetailsbox;
        private System.Windows.Forms.Label msgs_differ_label;
        private System.Windows.Forms.Button keygenbutton;
        private System.Windows.Forms.TextBox threadsbox;
        private System.Windows.Forms.Button keysizebutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_threads_warning;
        private System.Windows.Forms.Label label_keygen_running;
    }
}

