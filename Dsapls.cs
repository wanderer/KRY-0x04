﻿using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace KRY_0x04
{
    class Dsapls
    {
        internal string sha256sum_from_path(string path)
        {
            string sha256sum = "";
            try
            {
                FileStream my_filestream = File.OpenRead(path);
                SHA256Managed my_sha_256_managed = new SHA256Managed();
                byte[] byte_array_of_sha256 = my_sha_256_managed.ComputeHash(my_filestream);
                sha256sum = BitConverter.ToString(byte_array_of_sha256).Replace("-", string.Empty).ToLower();

                my_filestream.Close();
                my_sha_256_managed.Clear();
            }
            catch (Exception e)
            {
                show_exc_msgbox(e.ToString());
            }
            return sha256sum;
        }


        internal string sha256sum_from_string(string text)
        {
            string sha256sum = "";
            try
            {
                SHA256Managed sha256_provider = new SHA256Managed();
                byte[] bytes = Encoding.UTF8.GetBytes(text);
                byte[] hash = sha256_provider.ComputeHash(bytes);
                sha256sum = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            }
            catch (Exception e)
            {
                show_exc_msgbox(e.ToString());
            }
            return sha256sum;
        }


        internal string get_file_metrics(string filename)
        {
            string metrics = "";
            metrics += $"created: " + $"{File.GetCreationTime(filename)}" + Environment.NewLine;
            metrics += $"file type: " + $"{Path.GetExtension(filename)}";
            return metrics;
        }


        internal BigInteger return_bigint_representation_of_message(string input)
        {
            byte[] part_of_message = Encoding.ASCII.GetBytes(input);
            return new BigInteger(part_of_message);
        }


        internal string return_string_representation_of_bigint(BigInteger bA_number)
        {
            byte[] decrypted_b_a = bA_number.ToByteArray();
            string decrypted_text = "";
            for (int i = 0; i < decrypted_b_a.Length; i++)
            {
                decrypted_text += Convert.ToString(Convert.ToChar(decrypted_b_a[i]));
            }
            return decrypted_text;
        }


        private void show_exc_msgbox(string e)
        {
            MessageBox.Show("https://xkcd.com/2200 \nerror: " + e, "this should never have happened", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
