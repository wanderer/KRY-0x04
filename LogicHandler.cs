﻿using System;
using System.IO;
using System.IO.Compression;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRY_0x04
{
    class LogicHandler
    {
        Dsapls dsapls = new Dsapls();
        Rsa rsapls = new Rsa();


        internal void load_msg(RichTextBox msgbox, TextBox msgpathbox, TextBox msgdetailsbox, Label msgs_differ_label)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "pick a msg";
            /* freedom for all */
            dlg.Filter = "message (*.msg)|*.msg|custom (*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                msgbox.Text = File.ReadAllText(dlg.FileName);
                /* hide the label as it's show "on text changed" but since
                 * we're just loading a new message, informing us that it
                 * changed is not really relevant */
                msgs_differ_label.Visible = false;
                msgpathbox.Text = dlg.FileName;
                string f = dlg.FileName;
                msgdetailsbox.Text = dsapls.get_file_metrics(f);
            }
        }


        internal void save_msg(RichTextBox msgbox)
        {
            if (msgbox.Text == "")
            {
                MessageBox.Show("you've nothing to dump, friend ()");
                return;
            }
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            saveFileDialog1.Title = "export msg";
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.CheckPathExists = false;
            saveFileDialog1.DefaultExt = "";
            saveFileDialog1.Filter = "msg (*.msg)|*.msg|any file you wish (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(saveFileDialog1.FileName, msgbox.Text);
                    MessageBox.Show($"saved to {saveFileDialog1.FileName}", "gj, file saved!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error: " + ex.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        internal void do_msgs_differ(string msg, string path, Label do_they_differ, TextBox sha256sumbox)
        {
            if (msg != "" && path != "")
            {
                string msg_hash = dsapls.sha256sum_from_string(msg);
                string file_msg_hash = dsapls.sha256sum_from_path(path);
                if (msg_hash.Equals(file_msg_hash))
                {
                    do_they_differ.Visible = false;
                    sha256sumbox.Text = msg_hash;
                }
                else
                {
                    do_they_differ.Visible = true;
                    sha256sumbox.Text = msg_hash;
                }
            } else if (msg != "")
            {
                do_they_differ.Visible = false;
                string msg_hash = dsapls.sha256sum_from_string(msg);
                sha256sumbox.Text = msg_hash;
            } else if (msg == "")
            {
                do_they_differ.Visible = false;
                string msg_hash = dsapls.sha256sum_from_string(msg);
                sha256sumbox.Text = msg_hash;
            }
        }


        internal void load_privkey(RichTextBox privkeybox, TextBox privkeypathbox)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "pick a privkey";
            /* freedom for all */
            dlg.Filter = "private key (*.pri)|*.pri|custom (*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                privkeybox.Text = File.ReadAllText(dlg.FileName);
                privkeypathbox.Text = dlg.FileName;
            }
        }


        internal void load_pubkey(RichTextBox pubkeybox, TextBox pubkeypathbox)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "pick a pubkey";
            /* freedom for all */
            dlg.Filter = "public key (*.pub)|*.pub|custom (*.*)|*.*";
            dlg.CheckFileExists = false;
            dlg.CheckPathExists = false;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pubkeybox.Text = File.ReadAllText(dlg.FileName);
                pubkeypathbox.Text = dlg.FileName;
            }
        }



        internal void do_zip(RichTextBox msgbox, TextBox msgpathbox, Label msgs_differ_label)
        {
            if (msgpathbox.Text == "")
            {
                MessageBox.Show("load a msg file first, please", "oh hey");
                return;
            }
            else if (msgs_differ_label.Visible == true)
            {
                MessageBox.Show("msg changed.\nsave changes first, please", "oh hey");
                return;
            }

            MessageBox.Show("you're going to pick a sign file and a path where the final zip containing the signature and message will be placed.", "info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            string sign_path = "";
            OpenFileDialog dlg_sign = new OpenFileDialog();
            dlg_sign.Title = "pick a .sign file";
            dlg_sign.CheckFileExists = false;
            dlg_sign.CheckPathExists = false;
            dlg_sign.DefaultExt = "";
            dlg_sign.Filter = "sign file (*.sign)|*.sign|custom (*.*)|*.*";
            dlg_sign.FilterIndex = 1;
            dlg_sign.RestoreDirectory = true;

            DialogResult ds = dlg_sign.ShowDialog();
            if (ds == DialogResult.OK)
            {
                sign_path = dlg_sign.FileName;
            }
            else if (ds == DialogResult.Cancel)
            {
                MessageBox.Show("error: you have to choose a signature file to proceed", "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.ShowNewFolderButton = true;
            dlg.Description = "choose a folder where the zip will be placed";
            DialogResult d = dlg.ShowDialog();
            if (d == DialogResult.OK)
            {
                string pathTo_dirToZip = dlg.SelectedPath;
                /* the way we're handling this, it needs to be our workfolder,
                 * really, but at the same time must not be touched, so the way
                 * to go is to create another, disposable directory inside there */
                pathTo_dirToZip += Path.DirectorySeparatorChar + "disposable_workdir_pls";

                Directory.CreateDirectory(pathTo_dirToZip);
                string path_to_msg = msgpathbox.Text;
                string msg_filename = Path.GetFileName(path_to_msg);
                string sign_filename = Path.GetFileName(sign_path);
                try
                {
                    File.WriteAllText(pathTo_dirToZip + Path.DirectorySeparatorChar + msg_filename, msgbox.Text);
                    File.WriteAllText(pathTo_dirToZip + Path.DirectorySeparatorChar + sign_filename, msgbox.Text);
                }
                catch (Exception e)
                {
                    MessageBox.Show("error: " + e.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                try
                {
                    string filename = Path.Combine(pathTo_dirToZip + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar, "zipfile.zip");
                    if (File.Exists(filename))
                    {
                        MessageBox.Show($"file exists: {filename}\ndeleting", "ayy");
                        File.Delete(filename);
                    }

                    /* 0 for optimal compression, false for not including base dir */
                    ZipFile.CreateFromDirectory(pathTo_dirToZip, filename, 0, false);
                    MessageBox.Show("great success!", "gj, zip created");
                }
                catch (IOException exception)
                {
                    MessageBox.Show("error: " + exception.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Directory.Delete(pathTo_dirToZip, true);
            }
            else if (d == DialogResult.Cancel)
            {
                MessageBox.Show("error: you have to choose a path where the final zip will be placed", "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }


        internal void do_unzip()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "choose a zip file to unzip";
            dlg.Filter = "zip (*.zip)|*.zip|custom (*.*)|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string zipfile = dlg.FileName;
                FolderBrowserDialog dlg_folder = new FolderBrowserDialog();

                dlg_folder.Description = "select folder to unzip the zip to yeeeaaah";
                DialogResult dr = dlg_folder.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    string pathTo_UnzippedDir = dlg_folder.SelectedPath + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(zipfile);

                    if (Directory.Exists(pathTo_UnzippedDir))
                    {
                        pathTo_UnzippedDir = pathTo_UnzippedDir + "-NEW-" + DateTime.Now.ToString("yyyyMMddTHHmmss");
                        MessageBox.Show("dir exists, created a new one with the same name + date", "just so you know");
                        Directory.CreateDirectory(pathTo_UnzippedDir);
                    }

                    try
                    {
                        ZipFile.ExtractToDirectory(zipfile, pathTo_UnzippedDir);
                        MessageBox.Show("great success!", "gj, archive unzipped");
                    }
                    catch (IOException exc)
                    {
                        MessageBox.Show("error: " + exc.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }


        internal void do_sign(RichTextBox msgbox, RichTextBox privkeybox, RichTextBox pubkeybox, TextBox sha256sumbox)
        {
            if (msgbox.Text == "" || privkeybox.Text == "" || pubkeybox.Text == "" || sha256sumbox.Text == "")
            {
                MessageBox.Show("please, fill in all of the required fields");
                return;
            }
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string[] keys = privkeybox.Text.Split(' ');
            MessageBox.Show("we good?");
            Console.WriteLine(BigInteger.Parse(keys[1]));
            Console.WriteLine(BigInteger.Parse(keys[0]));
            string signed = Convert.ToString(BigInteger.ModPow(dsapls.return_bigint_representation_of_message(sha256sumbox.Text), BigInteger.Parse(keys[1]), BigInteger.Parse(keys[0])));


            saveFileDialog1.Title = ".sign";
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.CheckPathExists = false;
            saveFileDialog1.DefaultExt = "";
            saveFileDialog1.Filter = "sign file (*.sign)|*.sign|custom (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(saveFileDialog1.FileName, signed);
                    MessageBox.Show($"saved to {saveFileDialog1.FileName}", "gj, file saved!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error: " + ex.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        internal void do_verify_wrapper(RichTextBox privkeybox, RichTextBox pubkeybox)
        {
            if (privkeybox.Text != "" && pubkeybox.Text != "")
            {
                do_verify(pubkeybox);
            }
            else
            {
                MessageBox.Show("error: privkey, pubkey or both are not set. load your keys before proceeding.", "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void do_verify(RichTextBox pubkeybox)
        {
            string[] keys = pubkeybox.Text.Split(' ');
            string verified = "";
            string orig_message = "";
            bool proceed = true;

            OpenFileDialog dlg_sign = new OpenFileDialog();
            dlg_sign.Title = "pick a .sign file";
            /* freedom for all */
            dlg_sign.Filter = "sign file (*.sign)|*.sign|custom (*.*)|*.*";

            MessageBox.Show("you'll be choosing a signature and then a message file to verify", "info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            DialogResult d = dlg_sign.ShowDialog();
            if (d == DialogResult.OK)
            {
                try
                {
                    verified = dsapls.return_string_representation_of_bigint(BigInteger.ModPow(BigInteger.Parse(File.ReadAllText(dlg_sign.FileName)), BigInteger.Parse(keys[1]), BigInteger.Parse(keys[0])));
                }
                catch (Exception excep)
                {
                    MessageBox.Show("error: " + excep.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            } else if (d == DialogResult.Cancel)
            {
                proceed = false;
                MessageBox.Show("error: you have to choose a signature file to proceed", "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (proceed)
            {
                OpenFileDialog dlg_msg = new OpenFileDialog();

                dlg_msg.Title = "pick a .msg file";
                /* freedom for all */
                dlg_msg.Filter = "msg file (*.msg)|*.msg|custom (*.*)|*.*";

                DialogResult dr = dlg_msg.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    try
                    {
                        orig_message = dlg_msg.FileName;
                    }
                    catch (Exception excep)
                    {
                        MessageBox.Show("error: " + excep.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (dr == DialogResult.Cancel)
                {
                    proceed = false;
                    MessageBox.Show("error: you have to choose a msg file to proceed", "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (proceed)
                {
                    if (dsapls.sha256sum_from_path(orig_message) == verified)
                    {
                        MessageBox.Show($"{dsapls.sha256sum_from_path(orig_message)} == {verified}", "we good, signatures match!");
                    }
                    else
                    {
                        Console.WriteLine($"orig message: {orig_message}");
                        Console.WriteLine($"verif message: {verified}");
                        MessageBox.Show($"something is seriously wrong\n{dsapls.sha256sum_from_path(orig_message)} != {verified}", "checksums differ!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }


        internal void threads_tb_check(TextBox tb, Label threads_warning)
        {
            rsapls.tb_check(tb, threads_warning);
        }


        internal void keysize_handler(Button keysizebutton)
        {
            rsapls.keysize_handler(keysizebutton);
        }


        internal bool keygen_is_running { get; private set; }
        internal async Task genprimes_handler(TextBox threadsbox, Label threads_warning, Label keygen_running)
        {
            MessageBox.Show("in the next step, you will be choosing path for saving the key pair", "info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            string path = "";

            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.Description = "pick a location for your new keypair";
            dlg.ShowNewFolderButton = true;

            DialogResult dr = dlg.ShowDialog();
            if (dr == DialogResult.OK)
            {
                try
                {
                    path = dlg.SelectedPath;
                }
                catch (Exception excep)
                {
                    MessageBox.Show("error: " + excep.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (dr == DialogResult.Cancel)
            {
                MessageBox.Show("error: you have to choose a path", "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            keygen_running.Visible = true;
            keygen_running.Update();
            keygen_is_running = true;
            await Task.Run(() => rsapls.genprimes_handler(threadsbox, threads_warning, path));
            keygen_is_running = false;
            keygen_running.Visible = false;
            keygen_running.Update();
        }
    }
}
