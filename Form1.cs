﻿using System;
using System.Windows.Forms;

namespace KRY_0x04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        LogicHandler lh = new LogicHandler();


        private void load_msg_button_Click(object sender, EventArgs e)
        {
            /* load msg */
            lh.load_msg(msgbox, msgpathbox, msgdetailsbox, msgs_differ_label);
        }

        private void save_msg_button_Click(object sender, EventArgs e)
        {
            /* save msg */
            lh.save_msg(msgbox);
        }


        private void load_privkey_button_Click(object sender, EventArgs e)
        {
            /* load privkey */
            lh.load_privkey(privkeybox, privkeypathbox);
        }

        private void load_pubkey_button_Click(object sender, EventArgs e)
        {
            /* load pubkey */
            lh.load_pubkey(pubkeybox, pubkeypathbox);
        }


        private void zip_button_Click(object sender, EventArgs e)
        {
            /* zip */
            lh.do_zip(msgbox, msgpathbox, msgs_differ_label);
        }
        private void unzip_button_Click(object sender, EventArgs e)
        {
            /* unzip */
            lh.do_unzip();
        }
        
        private void sign_button_Click(object sender, EventArgs e)
        {
            /* sign */
            lh.do_sign(msgbox, privkeybox, pubkeybox, sha256sumbox);
        }

        private void verify_button_Click(object sender, EventArgs e)
        {
            /* verify */
            lh.do_verify_wrapper(privkeybox, pubkeybox);
        }

        private void cleanALL_button_Click(object sender, EventArgs e)
        {
            msgpathbox.Text = "";
            msgbox.Text = "";
            msgdetailsbox.Text = "";
            privkeypathbox.Text = "";
            pubkeypathbox.Text = "";
            sha256sumbox.Text = "";
            privkeybox.Text = "";
            pubkeybox.Text = "";
            label_keygen_running.Visible = false;
        }

        private void msgbox_TextChanged(object sender, EventArgs e)
        {
            lh.do_msgs_differ(msgbox.Text, msgpathbox.Text, msgs_differ_label, sha256sumbox);
        }

        private void keysize_button_Click(object sender, EventArgs e)
        {
            lh.keysize_handler(keysizebutton);
        }

        private async void keygen_button_Click(object sender, EventArgs e)
        {
            if (lh.keygen_is_running)
            {
                MessageBox.Show("keygen is already running!\nsince it's a resource-intensive operation, you can only run one keygen instance at a time", "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            await lh.genprimes_handler(threadsbox, label_threads_warning, label_keygen_running);
        }

        private void threadsbox_TextChanged(object sender, EventArgs e)
        {
            /* threads textbox */
            lh.threads_tb_check(threadsbox, label_threads_warning);
        }
    }
}
